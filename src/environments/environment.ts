// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDiFFobZay37A3qX5ToDnymMwj0jPVQgd0",
    authDomain: "meus-videos-372e0.firebaseapp.com",
    databaseURL: "https://meus-videos-372e0.firebaseio.com",
    projectId: "meus-videos-372e0",
    storageBucket: "meus-videos-372e0.appspot.com",
    messagingSenderId: "202457867378",
    appId: "1:202457867378:web:dbc5aecb42a8613538ceeb",
    measurementId: "G-19JHS30CNB"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
