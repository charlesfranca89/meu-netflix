import { Component, OnInit } from '@angular/core';
import { MovieModel } from 'src/app/core/model/movie-model';
import { MovieService } from 'src/app/core/services/movie.service';
import { ActivatedRoute } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {
  id: number;
  movie: MovieModel;

  constructor(
    private movieService: MovieService,
    private route: ActivatedRoute,
    private camera: Camera
  ) { }

  ngOnInit() {
    this.id = this.route.snapshot.params.id;
    this.movieService.getMovieDetails(this.id).then(data => {
      this.movie = data;
    })
  }

  takePhoto() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    
    this.camera.getPicture(options).then((imageData) => {
     // imageData is either a base64 encoded string or a file URI
     // If it's base64 (DATA_URL):
     let base64Image = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
     // Handle error
    });
  }

  addToFavorit() {
    this.movieService.addFavorit(this.movie);
    this.movieService.getFavorits().then(movies => {
      console.log(movies);
    })
  }

}
