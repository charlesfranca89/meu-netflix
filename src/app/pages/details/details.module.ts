import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailsPageRoutingModule } from './details-routing.module';

import { DetailsPage } from './details.page';
import { GenreJoinerPipe } from 'src/app/shared/pipes/genre-joiner.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailsPageRoutingModule,
  ],
  declarations: [DetailsPage, GenreJoinerPipe]
})
export class DetailsPageModule {}
