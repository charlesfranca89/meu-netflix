import { Component, ViewChildren, QueryList, ViewChild, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MovieModel } from 'src/app/core/model/movie-model';
import { MovieSectionHorizontalComponent } from 'src/app/shared/components/shared-components/movie-section-horizontal/movie-section-horizontal.component';
import { MovieService } from 'src/app/core/services/movie.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  @ViewChildren("movieSection") movieSections: QueryList<MovieSectionHorizontalComponent>;
  @ViewChild("formulario") formulario: MovieSectionHorizontalComponent;
  favoritMovies: MovieModel[];

  sections:{title:string, path: string}[] = [
    {
      title: "Em Breve",
      path: "upcoming"
    },
    {
      title: "Melhor Classificados",
      path: "top_rated"
    },
    {
      title: "Mais Populares",
      path: "popular"
    }
  ]
  constructor(
    private route:Router,
    private movieService: MovieService
  ) {
  }

  ngOnInit(): void {
    this.movieService.getFavorits().then(data => {
      console.log(data);
      this.favoritMovies = data;
    });
  }

  movieClicked(movie: MovieModel) {
    this.route.navigate(['movie', movie.id]);
  }

  getSection2() {
    // this.movieSections.toArray()[2].salvar();
    // console.log(this.movieSections.toArray()[2].movies);
    alert(this.formulario.title);
  }

}
