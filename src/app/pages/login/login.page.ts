import { Component, OnInit } from '@angular/core';
import { AuthModel } from 'src/app/core/model/auth-model';
import { AuthService } from 'src/app/core/services/auth.service';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  auth: AuthModel = {} as AuthModel;

  constructor(
    private authService: AuthService,
    private navController:NavController 
  ) { }

  ngOnInit() {
  }

  signIn() {
    this.authService.signIn(this.auth).then(() => {
      this.navController.navigateRoot(['home']);
    }).catch(error => {
      console.log(error);
    });
  }

}
