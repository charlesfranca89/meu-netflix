import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieSectionHorizontalComponent } from './movie-section-horizontal/movie-section-horizontal.component';

@NgModule({
  declarations: [
    MovieSectionHorizontalComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MovieSectionHorizontalComponent
  ]
})
export class SharedComponentsModule { }
