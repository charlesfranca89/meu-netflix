import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit, OnChanges, SimpleChanges } from '@angular/core';
import { MovieModel } from 'src/app/core/model/movie-model';
import { MovieService } from 'src/app/core/services/movie.service';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-movie-section-horizontal',
  templateUrl: './movie-section-horizontal.component.html',
  styleUrls: ['./movie-section-horizontal.component.scss'],
})
export class MovieSectionHorizontalComponent implements OnInit, AfterViewInit, OnChanges {
  @Input() title:string = "Em Breve";
  @Input() path:string = "upcoming";
  @Output() onMovieClicked = new EventEmitter<MovieModel>();
  @Input() outsideMovies: MovieModel[];
  @Input() static: boolean;
  movies: MovieModel[];

  constructor(
    private movieService: MovieService,
    private platform: Platform
  ) { }
  ngOnChanges(changes: SimpleChanges): void {
    this.movies = this.outsideMovies;
  }
  ngAfterViewInit(): void {
    this.movies = this.outsideMovies;
  }

  ngOnInit() {
    if (!this.static) {
      this.movieService.getMoviesSection(this.path)
      .then(data => {
        this.movies = data.results;
      }).catch(error => {
        alert(error.message);
      });
    } else {
      this.movies = this.outsideMovies;
    }
  }

  movieClicked(movie: MovieModel) {
    this.onMovieClicked.emit(movie);
  }

  salvar() {
    alert('');
  }

}
