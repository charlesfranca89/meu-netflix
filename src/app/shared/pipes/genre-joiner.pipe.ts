import { Pipe, PipeTransform } from '@angular/core';
import { Genre } from 'src/app/core/model/movie-model';

@Pipe({
  name: 'genreJoiner'
})
export class GenreJoinerPipe implements PipeTransform {

  transform(genres: Genre[]): unknown {
    if (!genres || genres.length == 0) {
      return "";
    }

    return genres.map(g => g.name).join(" - ");
  }

}
