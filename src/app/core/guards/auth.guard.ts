import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private navController: NavController
  ) {}
  
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.authService.isAuthenticated().then(isAuthenticated => {
        if (isAuthenticated) {
          resolve(true);
          return;
        }
        this.navController.navigateRoot([`login`], {
          queryParams: {
            url: state.url
          }
        });
        resolve(false);
      })
    })
  }
  
}
