import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthModel } from '../model/auth-model';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    public auth: AngularFireAuth,
    private storage: Storage
  ) { }

  signIn(auth: AuthModel) {
    return new Promise((resolve, reject) => {
      this.auth.signInWithEmailAndPassword(
        auth.username, 
        auth.password
      ).then(credentials => {
        this.storage.set("auth", JSON.stringify(true));
        resolve(credentials);
      }).catch(error => {
        reject(error);
      })
    })
  }

  isAuthenticated(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.storage.get("auth").then(data => {
        const auth = data;
        if (!data) {
          resolve(false);
          return;
        }
        resolve(JSON.parse(data));
      });
    })
  }
}
