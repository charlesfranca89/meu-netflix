import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiResponse } from '../model/api-response';
import { MovieModel } from '../model/movie-model';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  constructor(
    private http: HttpClient,
    private afs: AngularFirestore
  ) { }

  getMoviesSection(path: string): Promise<ApiResponse> {
    return new Promise((resolve, reject) => {
      this.http
        .get<ApiResponse>(`https://api.themoviedb.org/3/movie/${path}?api_key=51e4e9d52532d389174b5252cd99d33d`)
        .subscribe(data => resolve(data), error => reject(error));
    });
  }

  getMovieDetails(id): Promise<MovieModel> {
    return new Promise((resolve, reject) => {
      this.http
        .get<MovieModel>(`https://api.themoviedb.org/3/movie/${id}?api_key=51e4e9d52532d389174b5252cd99d33d`)
        .subscribe(data => resolve(data), error => reject(error));
    });
  }

  addFavorit(movie: MovieModel) {
    this.afs.collection("favorit_movies").doc(movie.id.toString()).set(movie);
  }

  getFavorits(): Promise<MovieModel[]> {
    return new Promise((resolve, reject) => {
      const movies = [];
      this.afs.collection("favorit_movies").get().subscribe(data => {
        data.forEach(movie => {
          movies.push(movie.data());
        });
        resolve(movies);
      });
    })
    
  }
}
